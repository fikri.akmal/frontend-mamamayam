import React, { useEffect, useState } from 'react';
import Footer from '../components/Footer';
import Navbar from '../components/Navbar';
import { Container, Row, Col } from 'react-bootstrap';
import { Text, Image, Box, useMediaQuery, Link } from '@chakra-ui/react';
import '../css/landingpage.css';
import Carousel from '../components/Carousel';
import Card from '../components/Card';
import CustomButton from '../components/Button';
import { Link as ReactLink } from 'react-router-dom';
import { motion } from 'framer-motion';

const LandingPage = () => {
  const [isLargerThan1000] = useMediaQuery('(min-width:1000px)');
  const [menuList, setMenuList] = useState([]);
  var promoMenu = []
  var favoriteMenu = []

  const getMenu = async () => {
    const respAllMenu = await fetch(`https://mamayam-backend.herokuapp.com/api/product/`);
    const allMenuList = await respAllMenu.json();
    setMenuList(allMenuList)
  }

  useEffect(() => {
    getMenu()
  },[])

  var counter = 0
  var counterPromo = 0
  var counterFavorite = 0

  while (counter <= Math.min(3, menuList.length) && (counterPromo <= 3 || counterFavorite <= 3) && menuList.length != 0){
    const menuNow = menuList[counter];
    if(menuNow.tags.includes('PROMO')){
      promoMenu.push(
        <Col sm md lg className="d-flex justify-content-center pt-5">

              <Card
                title={menuNow.namaProduct}
                url={menuNow.imageUrl}
                description={menuNow.description}
                price={menuNow.harga}
                discount={menuNow.diskon}
                tags={menuNow.tags}
              />
            </Col>
      )
      counterPromo++
    }

    if(menuNow.tags.includes('HOT')){
      favoriteMenu.push(
        <Col sm md lg className="d-flex justify-content-center pt-5">
          <Card
            title={menuNow.namaProduct}
            url={menuNow.imageUrl}
            description={menuNow.description}
            price={menuNow.harga}
            discount={menuNow.diskon}
            tags={menuNow.tags}
          />
        </Col>
      )
      counterFavorite++
    }
    counter++;
  }

  useEffect(() => {
    if (isLargerThan1000) {
      setBgConfig();
    } else {
      removeBgConfig();
    }

    return function cleanup() {
      removeBgConfig();
    };
  });

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ delay:0.2, duration: 0.5 }}
    >
      <Navbar logo="putih" />
      <Container fluid className={`welcome-section`}>
        <Row
          className={`${
            !isLargerThan1000
              ? 'd-flex flex-column justify-content-center align-items-center'
              : ''
          }`}
        >
          <Col className="d-flex flex-column justify-content-center">
            <Text ml="3vw" className="welcome-text">
              Welcome!
            </Text>
            <Box w="40vw">
              <Text ml="3vw" className="welcome-desc">
                We serve delicious food with the best quality ingredients. Brave
                enough to taste our hot specialist food? Feel free to order!
              </Text>
            </Box>
          </Col>

          <Col>
            <Image
              src="https://cdn.discordapp.com/attachments/861914096810197042/863208984218304512/AyamLanding.png"
              w="30vw"
              h="auto"
              ml="3vw"
              pt="2vh"
            />
          </Col>
        </Row>
      </Container>
      <Carousel />

      <Container
        fluid
        className="promo-section d-flex flex-column align-items-center"
      >
        <Row className="promo-title-wrapper">
          <Text w="100%" className="promo-title">
            Promo Menus
          </Text>
        </Row>
        <Container className="menu-wrapper" fluid>
          <Row className="d-flex justify-content-center">
            {promoMenu}
          </Row>
        </Container>

        <div style={{ marginTop: '2vh' }}>
          <Link as={ReactLink} to="/menu">
            <CustomButton>See more</CustomButton>
          </Link>
        </div>
      </Container>

      <Container
        fluid
        className="promo-section d-flex flex-column align-items-center"
      >
        <Row className="promo-title-wrapper">
          <Text w="100%" className="promo-title">
            Customer's Favorite
          </Text>
        </Row>
        <Container className="menu-wrapper" fluid>
          <Row className="d-flex justify-content-center">
            {favoriteMenu}
          </Row>
        </Container>

        <div style={{ marginTop: '2vh', marginBottom: '10vh' }}>
          <Link as ={ReactLink} to="/menu">
            <CustomButton>See more</CustomButton>
          </Link>
          
        </div>
      </Container>
      <Footer className="footer" />
      </motion.div>
  );
};

const setBgConfig = () => {
  document.body.style.backgroundImage =
    'url("https://cdn.discordapp.com/attachments/861914096810197042/863213530039582780/BgMerah.png")';
  document.body.style.backgroundRepeat = 'no-repeat';
  document.body.style.backgroundPosition = 'right top';
  document.body.style.backgroundSize = '30% auto';
  document.body.style.backgroundColor = '#F5EFEF';
};

const removeBgConfig = () => {
  document.body.style.backgroundImage = 'none';
};

export default LandingPage;
