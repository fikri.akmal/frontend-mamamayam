import React from 'react';
import { Row, Container } from 'react-bootstrap';
import HistoryCard from '../components/HistoryCard';
import Sidebar from '../components/Sidebar';
import '../css/dashboard.css';
import {motion} from 'framer-motion';


class Dashboard extends React.Component{
  constructor(){  
    super();
    this.state={
      data:[]
    };
    document.title = 'Mamam Ayam Order List';
    document.body.style.backgroundColor = '#F5EFEF';
  }

  fetchData(){
    fetch('https://mamayam-backend.herokuapp.com/api/cart/history/')
    .then(response=>response.json())
    .then((data)=>{
        this.setState({
            data:data
        });
    });  
  }

  componentDidMount(){
      this.fetchData();
  }

  
  render(){
    const empData=this.state.data;
    const cardRender=empData.map((emp)=>
      <div className="p-3">
        <HistoryCard 
          menu={emp.listBarang} 
          id={emp.idOrder} 
          status={emp.status} 
          date={emp.tanggal} 
          totalPrice={emp.totalHarga}
          />
      </div>        
    );
    return (
      <motion.div
                initial={{opacity:0}}
                animate={{opacity:1}}
                exit={{opaity:0}}
                transition={{duration:0.5}}
              >
        <Container fluid>
          <Row>
            <div className="p-0 sidebar">
            <Sidebar/>
            </div>
            <div className="boxContainer">
              <Container fluid className="header-wrapper pt-5">
                <div className="title-header pb-3">Order List</div>
                <hr className="divider" />
              </Container>
              {cardRender}
            </div>
          </Row>
        </Container>
      </motion.div>
    );
  }
}

export default Dashboard;
