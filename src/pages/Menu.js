import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import '../css/menu.css';
import Card from '../components/Card';
import CustSpinner from '../components/CustSpinner';

class Menu extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading: false,
    };
    document.title = 'Mamam Ayam Menu List';
    document.body.style.backgroundColor = '#F5EFEF';
  }

  fetchData() {
    this.setState({
      loading: true,
    });
    fetch('https://mamayam-backend.herokuapp.com/api/product/')
      .then(response => response.json())
      .then(data => {
        this.setState({
          data: data,
        });
      });
    this.setState({
      loading: false,
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const empData = this.state.data;
    const isLoading = this.state.loading;
    let promo = empData.filter(x => x.tags.includes('PROMO'));
    const promoRender = promo.map(emp => (
      <Col sm md lg className="d-flex justify-content-center pt-5">
        <Card
          title={emp.namaProduct}
          url={emp.imageUrl}
          description={emp.description}
          price={emp.harga}
          discount={emp.diskon}
          tags={emp.tags}
        />
      </Col>
    ));
    var custFave1 = empData.filter(x => x.tags.includes('HOT'));
    var custFave = custFave1.filter(x => promo.indexOf(x) < 0)
    const custFaveRender = custFave.map(emp => (
      <Col sm md lg className="d-flex justify-content-center pt-5">
        <Card
          title={emp.namaProduct}
          url={emp.imageUrl}
          description={emp.description}
          price={emp.harga}
          discount={emp.diskon}
          tags={emp.tags}
        />
      </Col>
    ));
    var makananFilter = empData.filter(x => custFave.indexOf(x) < 0)
    makananFilter = makananFilter.filter(x => promo.indexOf(x) < 0)
    const makanan = makananFilter.map(
      emp =>
        emp.tipeMakanan === 'Makanan' && (
          <Col sm md lg className="d-flex justify-content-center pt-5">
            <Card
              title={emp.namaProduct}
              url={emp.imageUrl}
              description={emp.description}
              price={emp.harga}
              discount={emp.diskon}
              tags={emp.tags}
            />
          </Col>
        )
    );

    const minuman = makananFilter.map(
      emp =>
        emp.tipeMakanan === 'Minuman' && (
          <Col sm md lg className="d-flex justify-content-center pt-5">
            <Card
              title={emp.namaProduct}
              url={emp.imageUrl}
              description={emp.description}
              price={emp.harga}
              discount={emp.diskon}
              tags={emp.tags}
            />
          </Col>
        )
    );
    return (
      <div>
        {isLoading ? <CustSpinner /> :
        <Container fluid className="menu-wrapper pb-5">
        <div className="boxContainer">
          <Container fluid className="title-wrapper p-5">
            <h1 className="title-menu">Menu List</h1>
            <div className="subTitle">The best food served fresh</div>
          </Container>
            <div>
              <Container fluid className="header-wrapper pt-5" id="promo">
                <div className="title-header text-center pb-3">Promo Menus</div>
                <hr className="divider" />
              </Container>
              <Container className="menu-wrapper" fluid>
                <Row className="d-flex justify-content-center">
                  {promoRender}
                </Row>
              </Container>
              <Container fluid className="header-wrapper pt-5" id="fave">
                <div className="title-header text-center pb-3">
                  Customer Favorite
                </div>
                <hr className="divider" />
              </Container>
              <Container className="menu-wrapper" fluid>
                <Row className="d-flex justify-content-center">
                  {custFaveRender}
                </Row>
              </Container>
              <Container fluid className="header-wrapper pt-5">
                <div className="title-header text-center pb-3">Main Course</div>
                <hr className="divider" />
              </Container>
              <Container className="menu-wrapper" fluid>
                <Row className="d-flex justify-content-center">{makanan}</Row>
              </Container>
              <Container fluid className="header-wrapper pt-5">
                <div className="title-header text-center pb-3">
                  Drinks & Dessert
                </div>
                <hr className="divider" />
              </Container>
              <Container className="menu-wrapper" fluid>
                <Row className="d-flex justify-content-center">{minuman}</Row>
              </Container>
            </div>
        </div>
      </Container>
      }
      </div>  
    );
  }
}

export default Menu;
