import React, { useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Box, Button } from '@chakra-ui/react';
import '../css/register.css';
import axios from 'axios';
import {
  useMediaQuery,
  Input,
  Text,
  Link,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  CloseButton,
} from '@chakra-ui/react';
import { Link as ReactLink, useHistory } from 'react-router-dom';
import LogoHitam from '../assets/images/LogoHitam.png';
import CustomButton from '../components/Button';
import styles from '../css/register.module.css';
import { motion } from 'framer-motion';

const Register = () => {
  const [isLargerThan900] = useMediaQuery('(min-width:900px)');
  const [username, setUsername] = useState('');
  const [fullname, setFullName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');
  const history = useHistory();
  const [registerLoading, setRegisterLoading] = useState(false);
  const [showAlert, setShowAlert] = useState(false);

  const createCart = async() => {
    const respCart = await fetch(
      `https://mamayam-backend.herokuapp.com/api/cart/shop/`
    );
    const cartList = await respCart.json();
    console.log(cartList)
    const cartFilter = cartList.filter(x => x.username == localStorage.getItem('user_id'));
    if(cartFilter.length == 0){
      // Bikin Cart
      await axios({
        method: 'post',
        url: 'https://mamayam-backend.herokuapp.com/api/cart/shop/',
        data: {
          "listBarang":[],
          "username":localStorage.getItem('user_id'),
        }
      })
    }
  };

  const getId = () => {
    const body = { username: username };
    fetch( "https://mamayam-backend.herokuapp.com/api/users/auth/get-user-id", {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type' : 'application/json',
      }
    }
    ).then(res => res.json())
    .then(res => {
      console.log("Fetch Done")
      const userId = res.userId;
      localStorage.setItem('user_id', userId);
      createCart();
    })
  };

  const handleCloseButton = () => {
    setShowAlert(false);
  };


  const registerValidation = res => {
    if (res.key !== undefined) {
      localStorage.setItem('token', res.key);
      localStorage.setItem('username', username);
      localStorage.setItem('isAdmin', false)
      getId()
      setRegisterLoading(false);
      history.push('/home');
    } else {
      setRegisterLoading(false);
      setShowAlert(true);
    }
  };

  const handleRegister = () => {
    if (password !== passwordConfirm) {
      alert('Password not match');
    } else {
      setRegisterLoading(true);
      // const url = 'http://127.0.0.1:8000/api/users/auth/register/';
      const url =
        'https://mamayam-backend.herokuapp.com/api/users/auth/register/';
      const body = {
        username: username,
        email: email,
        password1: password,
        password2: password,
      };

      fetch(url, {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        },
      })
        .then(res => res.json())
        .then(res => {
          registerValidation(res);
        });
    }
  };

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ delay: 0.2, duration: 0.5 }}
    >
      <motion.div
        style={{
          position: 'fixed',
          width: '500px',
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
          zIndex:(showAlert ? 999 : -999)
        }}
      >
        <Alert
          status="error"
          zIndex={showAlert ? 999 : -999}
          w={250}
          borderRadius="15px"
          mt={5}
          opacity={showAlert ? 1 : 0}
          transition="all 0.3s ease"
        >
          <AlertIcon />
          <Box flex="1">
            <AlertTitle>Error!</AlertTitle>
            <AlertDescription display="block">
              The form may not be filled correctly or the username password already exists
            </AlertDescription>
          </Box>
          <CloseButton
            position="absolute"
            right="8px"
            top="8px"
            onClick={handleCloseButton}
          />
        </Alert>
      </motion.div>
      <Container className={styles.login} fluid>
        <Row className="row-login">
          {isLargerThan900 && <Col className={styles.display_kiri}></Col>}

          <Col className="d-flex flex-column align-items-center">
            <Box mt="5vh">
              <img alt="Logo Mamam Ayam" src={LogoHitam}></img>
            </Box>

            <Text mt="10vh" className={styles.welcome_message}>
              Welcome to Mamayam
            </Text>

            <Input
              mt="8vh"
              variant="flushed"
              className="no-outline"
              placeholder="Username"
              onChange={e => setUsername(e.target.value)}
            />

            <Input
              mt="5vh"
              variant="flushed"
              className="no-outline"
              placeholder="Full Name"
              onChange={e => setFullName(e.target.value)}
            />

            <Input
              mt="5vh"
              variant="flushed"
              className="no-outline"
              placeholder="Email"
              onChange={e => setEmail(e.target.value)}
            />

            <Input
              mt="5vh"
              type="password"
              variant="flushed"
              className="no-outline"
              placeholder="Password"
              onChange={e => setPassword(e.target.value)}
            />

            <Input
              mt="5vh"
              type="password"
              variant="flushed"
              className="no-outline"
              placeholder="Confirm Password"
              onChange={e => setPasswordConfirm(e.target.value)}
              onKeyPress={(e) => {
                if (e.key === "Enter") {
                  handleRegister()
                }
              }}
            />

            <Link as={ReactLink} mt={2} className={styles.register_link} to="/">
              Already have an account? Login
            </Link>

            {registerLoading === false ? (
              <CustomButton
                onClick={handleRegister}
                className={styles.login_button}
              >
                Register
              </CustomButton>
            ) : (
              <Button
                style={{ borderRadius: '15px' }}
                mt="60px"
                mb="30px"
                w="160px"
                isLoading
                colorScheme="red"
                variant="outline"
                spinnerPlacement="end"
              />
            )}
          </Col>
        </Row>
      </Container>
    </motion.div>
  );
};

export default Register;
