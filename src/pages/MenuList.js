import React from 'react';
import { Row, Container, Col } from 'react-bootstrap';
import Sidebar from '../components/Sidebar';
import '../css/menu-list.css';
import MenuAdminCard from '../components/MenuAdminCard';
import AddMenuButton from '../components/AddMenuButton';
import { motion } from 'framer-motion';

class MenuList extends React.Component {
  constructor() {
    super();
    this.state = {
      data: [],
    };
    document.body.style.backgroundColor = '#F5EFEF';
  }

  fetchData() {
    fetch('https://mamayam-backend.herokuapp.com/api/product/')
      .then(response => response.json())
      .then(data => {
        this.setState({
          data: data,
        });
      });
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const empData = this.state.data;
    const menuRender = empData.map(emp => (
      <Col sm md lg className="d-flex justify-content-center pt-5">
        <MenuAdminCard
          title={emp.namaProduct}
          url={emp.imageUrl}
          description={emp.description}
          tipe={emp.tipeMakanan}
          price={emp.harga}
          discount={emp.diskon}
          tags={emp.tags}
          id={emp.idProduct}
        />
      </Col>
    ));
    return (
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        exit={{ opaity: 0 }}
        transition={{ duration: 0.5 }}
      >
        <Container fluid>
          <Row>
            <div className="p-0 sidebar">
              <Sidebar />
            </div>
            <div className="boxContainer">
              <Container fluid className="header-wrapper pt-5 pb-5">
                <Row>
                  <Col>
                    <div className="title-header pb-3">Menu List</div>
                  </Col>
                  <Col className="text-right">
                    <AddMenuButton className="statusButton" />
                  </Col>
                </Row>

                <hr className="divider" />
                <Row>{menuRender}</Row>
              </Container>
            </div>
          </Row>
        </Container>
      </motion.div>
    );
  }
}

export default MenuList;
