import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import Navbar from '../components/Navbar';
import { Button, Box, Image, Badge} from '@chakra-ui/react';
import '../css/cart.css';
import axios from 'axios';
import Footer from '../components/Footer';
import 'animate.css';
import CustSpinner from '../components/CustSpinner';
import {motion} from 'framer-motion';

class Cart extends React.Component {
  constructor() {
    super();
    this.state = {
      id:0,
      data: [],
      menuList: [],
      totalHarga: 0,
      usernameId:0,
      loading: false,
    };
    document.title = 'Cart List';
    document.body.style.backgroundColor = '#F5EFEF';
  }

  async removeMenu(id) {
    const respCart = await fetch(
      `https://mamayam-backend.herokuapp.com/api/cart/shop/`
    );
    const cartList = await respCart.json();
    const cartFilter = cartList.filter(x => x.username == localStorage.getItem('user_id'));
    if(cartFilter.length > 0){
      if (this.state.menuList.length > 1){
        const index = this.state.menuList.indexOf(Number(id)); 
        this.state.menuList.splice(index, 1);
      }else{
        this.setState({
          menuList: []
        })
      }
    }
    this.updateCart();
  }

  async submitOrder() {
    let date = new Date();
    console.log(date.getMonth())
    const dateFormat = `${date.getFullYear()}-${date.getMonth()}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
    await axios({
      method: 'post',
      url: 'https://mamayam-backend.herokuapp.com/api/cart/history/',
      data: {
        "listBarang":this.state.menuList,
        "totalHarga":this.state.totalHarga.toString(),
        "status":"Ordered",
        "username":localStorage.getItem('user_id'),
        "tanggal":dateFormat,
      }
    })

    await axios({
      method: 'patch',
      url: `https://mamayam-backend.herokuapp.com/api/cart/shop/${this.state.id}/`,
      data: {
        "listBarang": []
      }
    }).then(response => {
      window.location.href = "/home";
    });
  }

  async updateCart() {
    await axios({
      method: 'patch',
      url: `https://mamayam-backend.herokuapp.com/api/cart/shop/${this.state.id}/`,
      data: {
        listBarang: this.state.menuList,
      },
    }).then(response => {
      window.location.reload();
    });
  }

  async fetchData() {
    this.setState({
      loading: true,
    })
    const respCart = await fetch(
      `https://mamayam-backend.herokuapp.com/api/cart/shop/`
    );
    const cartList = await respCart.json();
    // console.log(cartList)
    const cartFilter = cartList.filter(x => x.username == localStorage.getItem('user_id'));
    if( cartFilter.length == 0){
    }else{
      
      this.setState({
        id: cartFilter[0].id,
        usernameId: cartFilter[0].username,
      });
      
      if(cartFilter[0].listBarang.length > 0){
        for (let i = 0; i < cartFilter[0].listBarang.length; i++) {
          this.setState({
            menuList: [...this.state.menuList, cartFilter[0].listBarang[i]],
          });
        }
        for (let i = 0; i < this.state.menuList.length; i++) {
          const resp = await fetch(
            `https://mamayam-backend.herokuapp.com/api/product/${this.state.menuList[i]}/`
          );
          const data = await resp.json();
          this.setState({
            data: [...this.state.data, data],
            totalHarga: this.state.totalHarga + parseInt(data.harga) - parseInt(data.diskon),
          });
        }
      }

      this.setState({
        loading: false,
      })
      console.log(this.state.menuList)
    }
    }


  componentDidMount() {
    this.fetchData();
  }

  render() {
    const empData = this.state.data;

    const renderMenu = empData.map(x => (
      <Col
        sm
        md
        lg
        className="d-flex justify-content-center pt-5"
        key={x.idProduct}
      >
        <Box
          maxW="sm"
          borderWidth="1px"
          borderRadius="25px"
          overflow="hidden"
          className="card"
          w="sm"
        >
          <Image
            objectFit="cover"
            src={x.imageUrl}
            className="components-image"
          />
          <Box p="6">
            <Box d="flex" alignItems="baseline">
              {x.tags.includes('PROMO') && (
                <Badge
                  borderRadius="full"
                  px="2"
                  colorScheme="merahGelap"
                  variant="solid"
                  mr="1"
                  className="badge"
                >
                  Promo
                </Badge>
              )}
              {x.tags.includes('NEW') && (
                <Badge
                  borderRadius="full"
                  px="2"
                  colorScheme="oranye"
                  variant="solid"
                  mr="1"
                  className="badge"
                >
                  New
                </Badge>
              )}
              {x.tags.includes('HOT') && (
                <Badge
                  borderRadius="full"
                  px="2"
                  colorScheme="kuning"
                  variant="solid"
                  mr="1"
                  className="badge"
                >
                  Hot
                </Badge>
              )}
            </Box>
            <Box
              mt="1"
              fontWeight="semibold"
              as="h1"
              lineHeight="tight"
              isTruncated
              fontSize="24px"
            >
              {x.namaProduct}
            </Box>
            <Box
              fontWeight="light"
              as="h1"
              lineHeight="tight"
              fontSize="6pxpx"
              maxH="70px"
              h="70px"
            >
              {x.description}
            </Box>
            <Box d="flex" mt="6" fontWeight="light" fontSize="16px">
              <Container>
                <Row>
                  {x.diskon > 0 ? (
                    <Col className="text-left p-0 d-flex align-items-center">
                      <s>Rp {x.harga}</s>{' '}
                      <Col className="pr-0 pl-1">Rp {x.harga - x.diskon}</Col>
                    </Col>
                  ) : (
                    <Col className="text-left p-0 d-flex align-items-center">
                      Rp {x.harga}
                    </Col>
                  )}
                  <Col className="p-0 text-right">
                    <Button
                      colorScheme="merahGelap"
                      variant="solid"
                      onClick={() => {
                        this.removeMenu(x.idProduct);
                      }}
                    >
                      Remove
                    </Button>
                  </Col>
                </Row>
              </Container>
            </Box>
          </Box>
        </Box>
      </Col>
    ));
    const isLoading = this.state.loading;
    return (
      <div>
          {isLoading ? 
          <CustSpinner />: 
              <motion.div
            initial={{ opacity: 0 }}
            animate={{ opacity: 1 }}
            exit={{ opaity: 0 }}
            transition={{ duration: 0.5 }}
          >
            <Container fluid className="p-0">
        <Navbar logo="hitam" />
        <Container fluid>
          <Container fluid className="title-wrapper p-5">
            <h1 className="title-menu">Food Cart</h1>
            <div className="subTitle">Serving the Best is Our Priority</div>
          </Container>
          <Row>{renderMenu}</Row>
          <Row className="p-5">
            <Button
              colorScheme="merahGelap"
              variant="solid"
              className="checkoutButton"
              onClick={this.submitOrder.bind(this)}
            >
              Checkout Order
            </Button>
          </Row>
        </Container>
        <Footer className=""/>
      </Container>
      </motion.div> }
      </div>
    );
  }
}

export default Cart;
