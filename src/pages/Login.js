import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Box, Button } from '@chakra-ui/react';
import {
  useMediaQuery,
  Input,
  Text,
  Link,
  Alert,
  AlertIcon,
  AlertTitle,
  AlertDescription,
  CloseButton,
} from '@chakra-ui/react';
import { Link as ReactLink, useHistory } from 'react-router-dom';
import LogoHitam from '../assets/images/LogoHitam.png';
import CustomButton from '../components/Button';
import styles from '../css/login.module.css';
import { motion } from 'framer-motion';

const Login = () => {
  const [isLargerThan900] = useMediaQuery('(min-width:900px)');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [loginLoading, setLoginLoading] = useState(false);
  const history = useHistory();
  const [showAlert, setShowAlert] = useState(false);

  const loginValidation = res => {
    const key = res.key;

    if (key !== undefined) {
      localStorage.setItem('token', key);
      localStorage.setItem('username', username);
      getId();
      checkRole()
    } else {
      setShowAlert(true);
    }

    setLoginLoading(false);
  };

  const getId = () => {
    const body = { username: username };
    fetch( "https://mamayam-backend.herokuapp.com/api/users/auth/get-user-id", {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type' : 'application/json',
      }
    }
    ).then(res => res.json())
    .then(res => {
      const userId = res.userId;
      localStorage.setItem('user_id', userId);
    })
  };

  const handleCloseButton = () => {
    setShowAlert(false);
  };

  const checkRole = () => {
    const urlCheckRole = "https://mamayam-backend.herokuapp.com/api/users/auth/check-role"
    // const urlCheckRole = 'http://localhost:8000/api/users/auth/check-role';
    const body = { username: username };
    fetch(urlCheckRole, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(res => {
        const isAdmin = res.isAdmin;
        // console.log("isAdmin:" + isAdmin)
        localStorage.setItem('isAdmin', isAdmin);
        if(localStorage.getItem('isAdmin') == 'true'){
          history.push("/dashboard")
        }else{
          history.push("/home")
        }
      });
  };

  const handleLogin = e => {
    // const url = 'http://127.0.0.1:8000/api/users/auth/login/';
    const url = 'https://mamayam-backend.herokuapp.com/api/users/auth/login/';
    const body = {
      username: username,
      password: password,
    };
    setLoginLoading(true);

    fetch(url, {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(res => {
        loginValidation(res);
      });

  };

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      history.push('/home');
    } 
  }, []);

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      transition={{ delay: 0.2, duration: 0.5 }}
    >
      <motion.div
        style={{
          position: 'fixed',
          width: '500px',
          display: 'flex',
          justifyContent: 'center',
          width: '100%',
          zIndex: showAlert ? 999 : -999
        }}
      >
        <Alert
          status="error"
          zIndex={showAlert ? 999 : -999}
          w={250}
          borderRadius="15px"
          mt={5}
          opacity={showAlert ? 1 : 0}
          transition="all 0.3s ease"
        >
          <AlertIcon />
          <Box flex="1">
            <AlertTitle>Error!</AlertTitle>
            <AlertDescription display="block">
              Wrong credential
            </AlertDescription>
          </Box>
          <CloseButton
            position="absolute"
            right="8px"
            top="8px"
            onClick={handleCloseButton}
          />
        </Alert>
      </motion.div>

      <Container className={styles.login} fluid>
        <Row className={styles.row_login}>
          {isLargerThan900 && <Col className={styles.display_kiri}></Col>}

          <Col className="d-flex flex-column align-items-center">
            <Box mt="5vh">
              <img alt="Logo Mamam Ayam" src={LogoHitam}></img>
            </Box>

            <Text mt="15vh" className={styles.welcome_message}>
              Welcome to Mamayam
            </Text>

            <Input
              mt="10vh"
              variant="flushed"
              className="no-outline"
              placeholder="Username"
              onChange={e => setUsername(e.target.value)}
            />

            <Input
              mt="5vh"
              type="password"
              variant="flushed"
              className="no-outline"
              placeholder="Password"
              onChange={e => setPassword(e.target.value)}
              onKeyPress={e => {
                if (e.key === 'Enter') {
                  handleLogin();
                }
              }}
            />

            <Link
              as={ReactLink}
              mt={2}
              className={styles.register_link}
              to="/register"
            >
              Don’t have an account? Register now
            </Link>

            {loginLoading === false ? (
              <CustomButton
                onClick={handleLogin}
                className={styles.login_button}
              >
                Login
              </CustomButton>
            ) : (
              <Button
                style={{ borderRadius: '15px' }}
                mt="60px"
                w="160px"
                isLoading
                colorScheme="red"
                variant="outline"
                spinnerPlacement="end"
              />
            )}
          </Col>
        </Row>
      </Container>
    </motion.div>
  );
};

export default Login;
