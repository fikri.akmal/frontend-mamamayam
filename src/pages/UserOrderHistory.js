import React, { useEffect, useState } from 'react';
import UserHistoryCard from '../components/UserOrderHistoryCard';
import Navbar from '../components/Navbar';
import { Flex, Text, Center } from '@chakra-ui/react';
import styles from '../css/user-order-history.module.css';
import Footer from '../components/Footer';
import { fetchHistory } from '../api';
import CustSpinner from '../components/CustSpinner';

const UserOrderHistory = () => {

    const [isLoading, setisLoading] = useState(true);
    const [orderData, setOrderData] = useState()

    const organizeData = (data) => {
        let list_data = []
        for (let i = 0; i<data.length; i++) {
            let dataObj = {}
            let foodArr = []
            
            for (let j = 0; j < data[i].listBarang.length; j++) {
                let foodObj = {}
                const currFood = data[i].listBarang[j]
                foodObj.nama_product = currFood.namaProduct
                foodObj.quantity = "1"
                foodObj.harga = currFood.harga
                foodArr.push(foodObj)
            }

            dataObj.list_barang = foodArr
            dataObj.total_harga = data[i].totalHarga
            dataObj.status = data[i].status
            dataObj.tanggal = data[i].tanggal
            dataObj.id = data[i].idOrder
            list_data.push(dataObj)
        }

        return list_data
    }
    
    useEffect(() => {
        document.body.style.backgroundColor = '#F5EFEF';
    }, []);

    useEffect(() => {
        async function getHistory() {
            const username = localStorage.getItem('username');
            const res = await fetchHistory(username);
            const dataOrder = organizeData(res.data)
            setOrderData([...dataOrder])
            setisLoading(false);
        }
        getHistory()
    }, []);


    return (
        isLoading ? <CustSpinner /> : 
        <>
            <Navbar logo="hitam" />
            <Center>
                <Flex flexDirection="column" mt={8} className={styles.title_wrapper}>
                    <Text className={styles.title}>Order History</Text>
                    <Text className={styles.subtitle} mt={-2}>
                        The best food served fresh
                    </Text>
                </Flex>
            </Center>

            {orderData.map((data, index) => {
                return (<Center mt={20}>
                    <UserHistoryCard key={`${index}`+100} data={data} />
                </Center>)
            })}

            <Footer className={styles.footer_down} />
        </>
    );
};

export default UserOrderHistory;
