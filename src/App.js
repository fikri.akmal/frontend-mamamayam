import React, { useEffect, useState } from 'react';
import { ChakraProvider, extendTheme } from '@chakra-ui/react';
import { Route, Switch, useLocation } from 'react-router-dom';
import Footer from './components/Footer';
import Login from './pages/Login';
import Menu from './pages/Menu';
import HistoryCard from './components/HistoryCard';
import Navbar from './components/Navbar';
import LandingPage from './pages/LandingPage';
import Dashboard from './pages/Dashboard';
import UserOrderHistory from './pages/UserOrderHistory';
import Register from './pages/Register'
import './css/global.css'
import {Helmet} from 'react-helmet';
import MenuList from './pages/MenuList'
import {AnimatePresence } from 'framer-motion';
import Cart from './pages/Cart';


function App() {
  const theme = extendTheme({
    colors: {
      cream: {
        50: '#F5EFEF',
        100: '#F5EFEF',
        200: '#F5EFEF',
        300: '#F5EFEF',
        400: '#F5EFEF',
        500: '#F5EFEF',
        600: '#F5EFEF',
        700: '#F5EFEF',
        800: '#F5EFEF',
        900: '#F5EFEF',
      },
      abuabu: {
        50: '#292929',
        100: '#292929',
        200: '#292929',
        300: '#292929',
        400: '#292929',
        500: '#292929',
        600: '#292929',
        700: '#292929',
        800: '#292929',
        900: '#292929',
      },
      kuning: {
        50: '#FF9E01',
        100: '#FF9E01',
        200: '#FF9E01',
        300: '#FF9E01',
        400: '#FF9E01',
        500: '#FF9E01',
        600: '#FF9E01',
        700: '#FF9E01',
        800: '#FF9E01',
        900: '#FF9E01',
      },
      merahGelap: {
        50: '#79081C',
        100: '#79081C',
        200: '#79081C',
        300: '#79081C',
        400: '#79081C',
        500: '#79081C',
        600: '#79081C',
        700: '#79081C',
        800: '#79081C',
        900: '#79081C',
      },
      oranye: {
        50: '#FF7000',
        100: '#FF7000',
        200: '#FF7000',
        300: '#FF7000',
        400: '#FF7000',
        500: '#FF7000',
        600: '#FF7000',
        700: '#FF7000',
        800: '#FF7000',
        900: '#FF7000',
      },
    },
  });

  const location = useLocation();
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    if (localStorage.getItem("token") != null) {
      setIsLoggedIn(true)
    } else {
      setIsLoggedIn(false)
    }
  }, [isLoggedIn])

  return (
    <ChakraProvider theme={theme}>
      <AnimatePresence exitBeforeEnter initial={false}>
        <Switch location={location} key={location.pathname}>
          <Route exact path="/" >
            {generateHelmet('Login')}
            <Login />
            <Footer />
          </Route>
          <Route exact path="/register">
            {generateHelmet('Register')}
            <Register />
            <Footer />
          </Route>

          <Route exact path="/menu-list">
            {generateHelmet('List Menu')}
            <MenuList />
            <Footer />
          </Route>


          <Route exact path="/menu">
            {generateHelmet('Menu')}
            <Navbar logo="hitam" />
            <Menu />
            <Footer />
          </Route>

            <Route exact path="/home">
              {generateHelmet("Mamam Ayam")}
              <LandingPage />
            </Route>
            
            <Route exact path="/menu">
              {generateHelmet("Menu")}
              <Navbar logo="hitam" /> 
              <Menu />
              <Footer />
            </Route>
            
            <Route exact path="/dashboard">
                <Dashboard />
                <Footer />
            </Route>
            
            <Route exact path="/cart">
              <Cart />
            </Route>
            
            <Route exact path="/history">
              <HistoryCard />
            </Route>
            
            <Route exact path="/user-history">
              {generateHelmet("Order History")}
              <UserOrderHistory />
            </Route>

          <Route exact path="/cart"></Route>

          <Route exact path="/history">
            <HistoryCard />
          </Route>

          <Route exact path="/user-history">
            {generateHelmet('Order History')}
            <UserOrderHistory />
          </Route>
        </Switch>
      </AnimatePresence>
    </ChakraProvider>
  );
}

const generateHelmet = title => {
  return (
    <Helmet>
      <title>{title}</title>
    </Helmet>
  );
};

export default App;
