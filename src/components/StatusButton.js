
import React, {useState} from 'react';
import {useHistory} from 'react-router-dom';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalCloseButton,
  ModalBody,
  Select,
  ModalFooter,
  Button,
  Box,
  useDisclosure,
} from '@chakra-ui/react';
import axios from 'axios';

const StatusButton = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [status, setStatus] = useState("");
  const history = useHistory();

  const changeStatus = async () => {
    let formField = new FormData();
    formField.append('status', status);

    await axios({
        method : 'patch',
        url : `https://mamayam-backend.herokuapp.com/api/cart/history/${props.id}/`,
        data : formField
    }).then((response) => {
        history.go(0)
    })
  }
  return (
    <Box>
        {props.variant === "mobile" && 
            <Button
            colorScheme="merahGelap"
            variant="solid"
            size="md"
            fontSize="21px"
            className="statusButton"
            onClick = {onOpen}
          >
            Change Status
          </Button>
        }
        {props.variant === "desktop" &&
        <Button colorScheme="merahGelap" variant="solid" onClick={onOpen}>
        Change Status
      </Button>
        }
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Change Order Status</ModalHeader>
          <ModalCloseButton />
          <ModalBody pb={6}>
            <Select 
                variant="flushed" 
                placeholder="Select New Status" 
                name="diskon" 
                onChange={(e) => setStatus(e.target.value)}>
              <option value="Ordered">Ordered</option>
              <option value="On Prepare">On Prepare</option>
              <option value="Served">Served</option>
              <option value="Canceled">Canceled</option>
            </Select>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="merahGelap" mr={3} onClick={changeStatus}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Box>
  );
};

export default StatusButton;
