import React, {useState, useEffect} from 'react';
import { Container, Row, Col} from 'react-bootstrap';
import '../css/card.css';
import {Box, Badge, Button, Image, useToast} from '@chakra-ui/react';
import {motion} from "framer-motion";
import axios from 'axios';

const Card = (props) => {
    const [isOrdered, setIsOrdered] = useState(false);
    const [cartId, setCartId] = useState(0);
    const [menuId, setMenuId] = useState(0);
    const [menuList, setMenuList] = useState([]);
    const toast = useToast();

    const fetchMenuList = async() => {
        const respAllMenu = await fetch(`https://mamayam-backend.herokuapp.com/api/product/`);
        const allMenuList = await respAllMenu.json();
        const selectMenu = allMenuList.filter(x => x.namaProduct == props.title)[0];

        setMenuId(selectMenu.idProduct);

        const respCart = await fetch(`https://mamayam-backend.herokuapp.com/api/cart/shop/`);
        const cartList = await respCart.json();
        const cartFilter = cartList.filter(x => x.username == localStorage.getItem('user_id'))[0];

        setCartId(cartFilter.id);
        setMenuList(cartFilter.listBarang);
    };

    const addMenu = async () => {
        const respCart = await fetch(`https://mamayam-backend.herokuapp.com/api/cart/shop/`);
        const cartList = await respCart.json();
        const cartFilter = cartList.filter(x => x.username == localStorage.getItem('user_id'))[0];
        console.log(cartFilter);

        await fetch(`https://mamayam-backend.herokuapp.com/api/product/`)
        .then(response => response.json())
        .then((data) => data.filter(x => x.namaProduct === props.title)[0])
        .then(data => setMenuList([...cartFilter.listBarang, data.idProduct]))

        const menuName = await (await fetch(`https://mamayam-backend.herokuapp.com/api/product/${menuId}/`)).json();

        toast({
            title: 'Menu Added.',
            description: `Menu ${menuName.namaProduct} added to cart`,
            status: 'success',
            duration: 9000,
            isClosable: true,
        })
    };

    const updateCart = async() => {
        await axios({
            method: 'patch',
            url: `https://mamayam-backend.herokuapp.com/api/cart/shop/${cartId}/`,
            data: {
                "listBarang" : menuList
            }
        })
    }
    
    useEffect(() => {
        fetchMenuList();
    }, []);
    useEffect(() => {
        if (menuList.find(x => x === menuId) ){
            updateCart();
            setIsOrdered(true);
        }
    }, [menuList]);

    return(
        <motion.div
            whileHover={{scale:1.1}}
        >
            <Box maxW="sm" borderWidth="1px" borderRadius="25px" overflow="hidden" className="card" w="sm">
            <Image objectFit="cover" src={props.url} className="components-image"/>
            <Box p="6">
                <Box d="flex" alignItems="baseline">
                    {props.tags.includes('PROMO') && <Badge borderRadius="full" px="2" colorScheme="merahGelap" variant="solid" mr="1" className="badge">
                        Promo
                    </Badge>}
                    {props.tags.includes('NEW') && <Badge borderRadius="full" px="2" colorScheme="oranye" variant="solid" mr="1" className="badge">
                        New
                    </Badge>}
                    {props.tags.includes('HOT') && <Badge borderRadius="full" px="2" colorScheme="kuning" variant="solid" mr="1" className="badge">
                        Hot
                    </Badge>}
                </Box>
                <Box mt="1" fontWeight="semibold" as="h1" lineHeight="tight" isTruncated fontSize="24px">
                {props.title}
                </Box>
                <Box fontWeight="light" as="h1" lineHeight="tight" fontSize="6pxpx" maxH="70px" h="70px">
                {props.description}
                </Box>
                <Box d="flex" mt="6"fontWeight="light" fontSize="16px">
                    <Container>
                        <Row>
                            {props.discount > 0 ? <Col className="text-left p-0 d-flex align-items-center"><s>Rp {props.price}</s> <Col className="pr-0 pl-1">Rp {props.price - props.discount}</Col>
                            </Col> :
                            <Col className="text-left p-0 d-flex align-items-center">
                            Rp {props.price}
                            </Col>
                            }
                            <Col className="p-0 text-right">
                                {!isOrdered && 
                                    <Button colorScheme="merahGelap" variant="solid" onClick={() => {addMenu()}}>
                                        Add
                                    </Button>
                                }
                                {isOrdered && 
                                    <Button colorScheme="merahGelap" variant="outline" >
                                        <a href='/cart/'>Remove</a>
                                    </Button>
                                }                                

                            </Col>
                        </Row>
                    </Container> 
                </Box>
            </Box>
        </Box>
        </motion.div>
        
    )
}

export default Card;