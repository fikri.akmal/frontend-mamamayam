import React from 'react';
import LogoPutih from '../assets/images/LogoPutih.png';
import '../css/footer.css';
import { Container, Row, Image } from 'react-bootstrap';
import { Box } from '@chakra-ui/react';

const Footer = props => {
  return (
    <Container className={'footer ' + props.className} fluid>
      <Row className="row-title d-flex justify-content-end">
        <Image alt="Logo Mamam Ayam" src={LogoPutih} fluid />
      </Row>
      <Row className="row-nama">
        <Box m={5}>
          <p>Fikri Akmal</p>
          <a href="https://instagram.com/fikrii_akmal">Instagram</a>
          <br />
          <a href="https://gitlab.com/fikri.akmal">Gitlab</a>
          <br />
          <a href="https://www.linkedin.com/in/fikriakmal/">LinkedIn</a>
        </Box>
        <Box m={5}>
          <p>Putu Wigunarta</p>
          <a href="https://instagram.com/putu.wig">Instagram</a>
          <br />
          <a href="https://gitlab.com/pwigunarta">Gitlab</a>
          <br />
          <a href="https://www.linkedin.com/in/putu-wigunarta-16ba57192/">
            LinkedIn
          </a>
        </Box>
      </Row>
    </Container>
  );
};

export default Footer;
