import React from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import '../css/navbar.css';
import { LinkContainer } from 'react-router-bootstrap';
import { useHistory } from 'react-router-dom';

const NavbarHitam = props => {
  const linkLogo =
    props.logo === 'hitam'
      ? 'https://cdn.discordapp.com/attachments/861914096810197042/862599193791823902/black_logo.png'
      : 'https://cdn.discordapp.com/attachments/861914096810197042/863214043854274590/LogoPutih.png';

  const history = useHistory();

  const handleLogout = e => {
    // const url = 'http://127.0.0.1:8000/api/users/auth/logout/';
    const url = 'https://mamayam-backend.herokuapp.com/api/users/auth/logout/';

    fetch(url, {
      method: 'POST',
      headers: {
        Authorization: `Token ${localStorage.getItem('token')}`,
      },
    })
      .then()
      .then(res => {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        localStorage.removeItem('isAdmin');
        localStorage.removeItem('user_id');
        history.push('/');
      });
  };

  return (
    <Navbar collapseOnSelect expand="lg" className="navbar">
      <Navbar.Toggle aria-controls="responsive-navbar-nav" />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link href="/menu" className="nav-text">
            Menu
          </Nav.Link>
          <Nav.Link href="/cart" className="nav-text">
            Cart
          </Nav.Link>
          <LinkContainer to="/user-history">
            <Nav.Link className="nav-text">History</Nav.Link>
          </LinkContainer>
          <LinkContainer to="/">
            <Nav.Link className="nav-text" onClick={handleLogout}>
              Logout
            </Nav.Link>
          </LinkContainer>
        </Nav>
        <Nav>
          <Navbar.Brand href="/home">
            <img
              alt=""
              src={linkLogo}
              width="120"
              className="d-inline-block align-top"
            />
          </Navbar.Brand>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
};

export default NavbarHitam;
