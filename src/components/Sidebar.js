import React from 'react';
import {
    CDBSidebar,
    CDBSidebarContent,
    CDBSidebarFooter,
    CDBSidebarHeader,
    CDBSidebarMenu,
    CDBSidebarMenuItem,
} from 'cdbreact';
import {NavLink} from 'react-router-dom';
import {Image} from '@chakra-ui/react'
import '../css/sidebar.css';
import logoPutih from '../assets/images/LogoPutih.png'
import { useHistory } from 'react-router-dom';

const Sidebar = () => {
  const history = useHistory();

  const handleLogout = e => {
    // const url = 'http://127.0.0.1:8000/api/users/auth/logout/';
    const url = 'https://mamayam-backend.herokuapp.com/api/users/auth/logout/';

    fetch(url, {
      method: 'POST',
      headers: {
        Authorization: `Token ${localStorage.getItem('token')}`,
      },
    })
      .then()
      .then(res => {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        localStorage.removeItem('isAdmin');
        localStorage.removeItem('user_id');
        history.push('/');
      });
  };

    return (
        <div
      style={{ display: 'flex', height: '100vh', overflow: 'scroll initial' }}
    >
      <CDBSidebar textColor="#fff" backgroundColor="#333" className="sidebarWrapper">
        <CDBSidebarHeader>
          <a
            href="/"
            className="text-decoration-none"
            style={{ color: 'inherit' }}
          >
            <div className="sidebarContainer">
              <Image className="sidebarBrandLogo" src={logoPutih}></Image>
            </div>
          </a>
        </CDBSidebarHeader>

        <CDBSidebarContent className="sidebar-content">
          <CDBSidebarMenu>
            <NavLink exact to="/dashboard" activeClassName="activeClicked" className="sidebar-node">
              <CDBSidebarMenuItem icon="columns">Order List</CDBSidebarMenuItem>
            </NavLink>
            <NavLink exact to="/menu-list" activeClassName="activeClicked" className="sidebar-node">
              <CDBSidebarMenuItem icon="table">Menu List</CDBSidebarMenuItem>
            </NavLink>
          </CDBSidebarMenu>
        </CDBSidebarContent>

        <CDBSidebarFooter style={{ textAlign: 'center' }} className="sidebarFooterWrapper">
          <div className="sidebarFooter">
            <div className="sidebarFooterTitle">
              {localStorage.getItem("username")}
            </div>
            <button onClick={handleLogout} activeClassName="activeClicked" className="sidebarFooterLogout">
              <CDBSidebarMenuItem>Log Out</CDBSidebarMenuItem>
            </button>
          </div>
        </CDBSidebarFooter>
      </CDBSidebar>
    </div>
    );
};

export default Sidebar;