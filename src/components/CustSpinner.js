import React from 'react';
import {motion} from 'framer-motion';
import {Row, Col} from 'react-bootstrap';
import {Image} from '@chakra-ui/react';
import LogoHitam from '../assets/images/LogoHitam.svg';

const spinner = {
    animation: {
        opacity: 0.1,
        transition: {
        duration: 1,
        yoyo: Infinity
        }
    }
}

const CustSpinner = () => {        
    return(
        <Row>
        <Col sm md lg className="spinner d-flex justify-content-center align-items-center">
            <div className="spinnerContainer text-center">
                <motion.div
                variants={spinner}
                animate = "animation"
                >
                <Image src={LogoHitam} className="spinnerPhotos" />
                </motion.div>
            </div>
        </Col>
    </Row>
    )
}

export default CustSpinner;