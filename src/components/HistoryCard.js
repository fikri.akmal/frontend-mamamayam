import React from 'react';
import '../css/historyCard.css';
import { Container, Row, Col } from 'react-bootstrap';
import {
  Box,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Table,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
} from '@chakra-ui/react';
import StatusButton from './StatusButton';

class HistoryCard extends React.Component {
    constructor(){
        super();
        this.state={
            data:[]
        };
    }
    async getMenu() {
        for(let i=0; i < this.props.menu.length; i++){
            const resp = await fetch(`https://mamayam-backend.herokuapp.com/api/product/${this.props.menu[i]}/`);
            const data = await resp.json();
            this.setState({
                data:[...this.state.data,data]
            });
        }
    }

    componentDidMount() {
        this.getMenu();
    }
    
    render(){
        return (
            <Box>
              <Box borderWidth="1px" maxW="1330px" p="6" className="historyCard">
                <Container fluid>
                  <Row>
                    <Col>
                      <Row className="title" fontWeight="semibold">
                        Order {this.props.id}
                      </Row>
                      <Row className="status">Status: {this.props.status}</Row>
                    </Col>
                    <Col className="date">{this.props.date}</Col>
                  </Row>
                  <Box pt="5">
                    <Accordion allowToggle className="accordion">
                      <AccordionItem style={{ borderTop: 0 }}>
                        <h2>
                          <AccordionButton>
                            <Box flex="1" textAlign="left">
                              Menu
                            </Box>
                            <AccordionIcon />
                          </AccordionButton>
                        </h2>
                        <AccordionPanel pb="4">
                          <Table variant="simple" w="100%">
                            <Thead className="thead">
                              <Tr className="tr">
                                <Th className="table text-center">Food</Th>
                                <Th className="table text-center">Quantity</Th>
                                <Th className="table text-center">Price</Th>
                              </Tr>
                            </Thead>
                            <Tbody>
                                { this.state.data.map((data, index) => {
                                    return(<Tr key={index}>
                                        <Td className="table text-center">{data.namaProduct}</Td>
                                        <Td className="table text-center">1</Td>
                                        <Td className="table text-center">{data.harga - data.diskon}</Td>
                                    </Tr>);
                                })}
                            </Tbody>
                          </Table>
                        </AccordionPanel>
                      </AccordionItem>
                    </Accordion>
                    <Box
                      d="flex"
                      mt="6"
                      alignItems="center"
                      fontWeight="light"
                      fontSize="24px"
                    >
                      <Container>
                        <Row>
                          <Col className="text-left p-0">Rp {this.props.totalPrice}</Col>
                          <Col className="p-0 text-right">
                            <StatusButton variant="desktop" id={this.props.id}/>
                          </Col>
                        </Row>
                      </Container>
                    </Box>
                  </Box>
                </Container>
              </Box>
              <Box maxW="300px" className="mobile-historyCard" p="5px">
                <Container fluid>
                  <div className="title">Order {this.props.id}</div>
                  <div className="status">Status: {this.props.status}</div>
                  <div className="date">{this.props.date}</div>
                  <Box pt="5">
                    <Accordion allowToggle className="accordion">
                      <AccordionItem>
                        <h2>
                          <AccordionButton>
                            <Box flex="1" textAlign="left">
                              Menu
                            </Box>
                            <AccordionIcon />
                          </AccordionButton>
                        </h2>
                        <AccordionPanel pb="4">
                          <Table variant="simple" size="xs">
                            <Thead>
                              <Tr>
                                <Th className="table text-center">Food</Th>
                                <Th className="table text-center">Quantity</Th>
                                <Th className="table text-center">Price</Th>
                              </Tr>
                            </Thead>
                            <Tbody>
                                { this.state.data.map((data, index) => {
                                    return(<Tr key={index}>
                                        <Td className="table text-center">{data.namaProduct}</Td>
                                        <Td className="table text-center">1</Td>
                                        <Td className="table text-center">{data.harga}</Td>
                                    </Tr>);
                                })}
                            </Tbody>
                          </Table>
                        </AccordionPanel>
                      </AccordionItem>
                    </Accordion>
                    <Box d="flex" mt="6" alignItems="center" fontWeight="light">
                      <Container>
                        <div className="text-center p-0 price">Total:</div>
                        <div className="text-center p-0 price">Rp {this.props.totalPrice}</div>
                        <div className="p-2 text-center">
                          <StatusButton variant="mobile" id={this.props.id}/>
                        </div>
                      </Container>
                    </Box>
                  </Box>
                </Container>
              </Box>
            </Box>
          );
    }
};

export default HistoryCard;
