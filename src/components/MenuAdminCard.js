import React from 'react';
import { Container, Row, Col} from 'react-bootstrap';
import '../css/adminMenuCard.css';
import {Box, Badge, Button, Image,Modal,
        ModalOverlay, ModalContent, ModalHeader,
        ModalFooter, ModalBody, ModalCloseButton,
        useDisclosure, Input, FormControl, FormLabel,
        Textarea} from '@chakra-ui/react';
import axios from 'axios';
import { useHistory } from 'react-router';
import { useState } from 'react';

const MenuAdminCard = (props) => {
    const history = useHistory()
    const[harga, setHarga] = useState(props.price);
    const[tags, setTags] = useState(props.tags.toString());
    const[diskon, setDiskon] = useState(props.discount);
    const[imageUrl, setImageUrl] = useState(props.url);
    const[description, setDescription] = useState(props.description);

    const deleteMenu = async () => {
        await axios({
            method : 'delete',
            url : `https://mamayam-backend.herokuapp.com/api/product/${props.id}/`
        }).then((response) => {
            console.log(response.data);
            history.go(0)
        })
    }

    const editMenuInfo = async () => {
        let formField = new FormData();
        formField.append('namaProduct', props.title);
        formField.append('tipeMakanan', props.tipe)
        
        if(harga !==0){
            formField.append('harga', harga);
        }
        
        if(tags !== ""){
            const myArr = tags.split(",");
            for(var i = 0; i < myArr.length; i++){
                formField.append('tags', myArr[i])
            }
        }
        formField.append('diskon', diskon);

        if(imageUrl !== ""){
            formField.append('imageUrl', imageUrl);
        }
        
        if(description !== ""){
            formField.append('description', description);
        }
    
        await axios({
            method : 'put',
            url : `https://mamayam-backend.herokuapp.com/api/product/${props.id}/`,
            data : formField
        }).then((response) => {
            history.go(0)
        })
    }

    const { isOpen, onOpen, onClose } = useDisclosure()
    return(
        <Box maxW="sm" borderWidth="1px" borderRadius="25px" overflow="hidden" className="card" w="sm">
            <Image objectFit="cover" src={props.url} className="components-image"/>
            <Box p="6">
                <Box d="flex" alignItems="baseline">
                    {props.tags.includes('PROMO') && <Badge borderRadius="full" px="2" colorScheme="merahGelap" variant="solid" mr="1" className="badge">
                        Promo
                    </Badge>}
                    {props.tags.includes('NEW') && <Badge borderRadius="full" px="2" colorScheme="oranye" variant="solid" mr="1" className="badge">
                        New
                    </Badge>}
                    {props.tags.includes('HOT') && <Badge borderRadius="full" px="2" colorScheme="kuning" variant="solid" mr="1" className="badge">
                        Hot
                    </Badge>}
                </Box>
                <Box mt="1" fontWeight="semibold" as="h1" lineHeight="tight" isTruncated fontSize="24px">
                {props.title}
                </Box>
                <Box fontWeight="light" as="h1" lineHeight="tight" fontSize="6pxpx" maxH="70px" h="70px">
                {props.description}
                </Box>
                <Box d="flex" mt="6"fontWeight="light" fontSize="16px">
                    <Container>
                        <Row>
                            {props.discount > 0 ? <Col className="text-left p-0 d-flex align-items-center"><s>Rp {props.price}</s> <Col className="pr-0 pl-1">Rp {props.price - props.discount}</Col>
                            </Col> :
                            <Col className="text-left p-0 d-flex align-items-center">
                            Rp {props.price}
                            </Col>
                            }
                            <Col className="p-0 text-right">
                                <Button colorScheme="merahGelap" variant="solid" onClick={onOpen}>
                                    Edit
                                </Button>
                            </Col>
                        </Row>
                    </Container> 
                </Box>
            </Box>
            <Modal
                isOpen={isOpen}
                onClose={onClose}
            >
                <ModalOverlay />
                <ModalContent>
                <ModalHeader>Edit Menu {props.title}</ModalHeader>
                <ModalCloseButton />
                <ModalBody pb={6}>
                        <FormControl mt={4}>
                        <FormLabel>Harga Produk</FormLabel>
                        <Input 
                            size="md" 
                            variant="flushed" 
                            className="input-edit-menu"
                            name="harga" 
                            value={harga}
                            onChange={(e) => setHarga(e.target.value)}
                            />
                        </FormControl>

                        <FormControl mt={4}>
                        <FormLabel>Diskon</FormLabel>
                        <Input 
                            variant="flushed" 
                            className="input-edit-menu"
                            name="diskon" 
                            value={diskon}
                            onChange={(e) => setDiskon(e.target.value)}
                            />
                        </FormControl>

                        <FormControl mt={4}>
                        <FormLabel>Image Url</FormLabel>
                        <Input 
                            variant="flushed" 
                            className="input-edit-menu"
                            name="imageUrl" 
                            value={imageUrl}
                            onChange={(e) => setImageUrl(e.target.value)}
                            />
                        </FormControl>

                        <FormControl mt={4}>
                        <FormLabel>Tags</FormLabel>
                        <Input 
                            variant="flushed"
                            className="input-edit-menu"
                            name="tags" 
                            value={tags}
                            onChange={(e) => setTags(e.target.value)}
                            />
                        </FormControl>

                        <FormControl mt={4}>
                        <FormLabel>Description</FormLabel>
                        <Textarea
                            className="input-edit-menu textarea-input chakra-input"
                            name="description" 
                            value={description}
                            onChange={(e) => setDescription(e.target.value)}
                            overflowY="hidden"
                            />
                        </FormControl>
                </ModalBody>
                <ModalFooter>
                    <Button colorScheme="merahGelap" mr={3} onClick={editMenuInfo}>
                    Save
                    </Button>
                    <Button onClick={deleteMenu}>Delete</Button>
                </ModalFooter>
                </ModalContent>
            </Modal>
        </Box>
    )
}

export default MenuAdminCard;