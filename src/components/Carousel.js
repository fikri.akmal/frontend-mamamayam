import React, { useState } from 'react'
import '../css/carousel.css';
import LeftArrow from '../assets/images/LeftArrow.png'
import RightArrow from '../assets/images/RightArrow.png'

const Carousel = () => {

    const imageData = [
        {
            address:"https://cdn.discordapp.com/attachments/861914096810197042/861924778578018324/image_1_2.png",
            title:"Katsu Geprek",
            description:"The Tendercy of the Chicken will bring you the vibes of the good old homecook’s that people want. With The Spice of Chille Sauce that brings the variant into the taste"
        },
        {
            address:"https://images.unsplash.com/photo-1599154456742-c82164d2dfb0?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1189&q=80",
            title:"Katsu Geprek",
            description:"The Tendercy of the Chicken will bring you the vibes of the good old homecook’s that people want. With The Spice of Chille Sauce that brings the variant into the taste"
        },
        {
            address:"https://images.unsplash.com/photo-1610827776793-389a26d9949f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80",
            title:"Katsu Geprek",
            description:"The Tendercy of the Chicken will bring you the vibes of the good old homecook’s that people want. With The Spice of Chille Sauce that brings the variant into the taste"
        }   
    ]

    const length = imageData.length;
    const [imageIdx, setImageIdx] = useState(0);

    const nextSlide = () => {
        if (imageIdx+1 === length) {
            setImageIdx(0)
        } else {
            setImageIdx(imageIdx+1)
        }
    }

    const prevSlide = () => {
        if (imageIdx-1 < 0) {
            setImageIdx(length-1)
        } else {
            setImageIdx(imageIdx-1)
        }
    }

    return (
        <section className="slider">
            <img alt="" className="image" src={imageData[imageIdx].address}></img>
            <button className="left-button" onClick={prevSlide}>
                <img alt="" src={LeftArrow}></img>
            </button>
            <button className="right-button" onClick={nextSlide}>
                <img alt="" src={RightArrow}></img>
            </button>
            <div className="image-info">
                <h1 className="image-title">{imageData[imageIdx].title}</h1>
                <p className="image-desc">{imageData[imageIdx].description}</p>
            </div>
        </section>
    )   
}




export default Carousel;
