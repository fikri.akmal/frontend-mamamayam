import React, { useState } from 'react';
import '../css/adminMenuCard.css';
import {
  Button,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  useDisclosure,
  FormControl,
  FormLabel,
  Input,
  Textarea,
} from '@chakra-ui/react';
import { useHistory } from 'react-router-dom';
import axios from 'axios';

const AddMenuButton = (props) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [namaProduct, setNamaProduct] = useState('');
  const [harga, setHarga] = useState(0);
  const [tipeMakanan, setTipeMakanan] = useState('');
  const [tags, setTags] = useState('');
  const [diskon, setDiskon] = useState(0);
  const [imageUrl, setImageUrl] = useState('');
  const [description, setDescription] = useState('');
  const history = useHistory();

  const addMenuInfo = async () => {
    let formField = new FormData();
    formField.append('namaProduct', namaProduct);
    formField.append('harga', harga);
    formField.append('tipeMakanan', tipeMakanan);
    const myArr = tags.split(',');
    for (var i = 0; i < myArr.length; i++) {
      formField.append('tags', myArr[i]);
    }
    formField.append('diskon', diskon);
    formField.append('imageUrl', imageUrl);
    formField.append('description', description);

    await axios({
      method: 'post',
      url: 'https://mamayam-backend.herokuapp.com/api/product/',
      data: formField,
    }).then(response => {
      history.go(0);
    });
  };

  return (
    <div>
      <Button
        colorScheme="merahGelap"
        variant="solid"
        size="md"
        fontSize="21px"
        className={props.className ? props.className : 'statusButton'}
        onClick={onOpen}
      >
        Add Menu
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Add New Menu</ModalHeader>
          <ModalBody>
            <FormControl>
              <FormLabel>Nama Produk</FormLabel>
              <Input
                size="md"
                variant="flushed"
                className="input-edit-menu"
                name="namaProduct"
                value={namaProduct}
                onChange={e => setNamaProduct(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Tipe Produk</FormLabel>
              <Input
                size="md"
                variant="flushed"
                className="input-edit-menu"
                name="tipeMakanan"
                value={tipeMakanan}
                onChange={e => setTipeMakanan(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Harga Produk</FormLabel>
              <Input
                size="md"
                variant="flushed"
                className="input-edit-menu"
                name="harga"
                value={harga}
                onChange={e => setHarga(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Diskon</FormLabel>
              <Input
                variant="flushed"
                className="input-edit-menu"
                name="diskon"
                value={diskon}
                onChange={e => setDiskon(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Image Url</FormLabel>
              <Input
                variant="flushed"
                className="input-edit-menu"
                name="imageUrl"
                value={imageUrl}
                onChange={e => setImageUrl(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Tags</FormLabel>
              <Input
                variant="flushed"
                className="input-edit-menu"
                name="tags"
                value={tags}
                onChange={e => setTags(e.target.value)}
              />
            </FormControl>

            <FormControl mt={4}>
              <FormLabel>Description</FormLabel>
              <Textarea
                className="input-edit-menu textarea-input chakra-input"
                name="description"
                value={description}
                onChange={e => setDescription(e.target.value)}
                type="textarea"
              />
            </FormControl>
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="merahGelap" mr={3} onClick={addMenuInfo}>
              Save
            </Button>
            <Button onClick={onClose}>Cancel</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};

export default AddMenuButton;
