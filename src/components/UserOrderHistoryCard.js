import React from 'react'
import {
    Accordion,
    AccordionItem,
    AccordionButton,
    AccordionPanel,
    AccordionIcon,
    Box,
    Flex,
    Text,
    Table,
    Thead,
    Tbody,
    Tr,
    Th,
    Td
} from "@chakra-ui/react"
import style from '../css/userOrderHistoryCard.module.css'



const UserOrderHistoryCard = ({data}) => {

    console.log(data)

    return (
        <Box w="80%">
            <Box p="30px" bg="white" className={style.mobile_card_wrapper}>
                <Flex justifyContent="space-between" w="100%">
                    <Text className={style.title}>
                        Order {data.id}
                    </Text>
                    <Text className={style.title}>
                        {data.tanggal.split(" ")[0]}
                    </Text>
                </Flex>
                <span className={style.status}>
                    Status: {data.status}
                </span>
                <Accordion defaultIndex={[0]} allowMultiple mt={5}>
                    <AccordionItem style={{borderTop: 0}}>
                        <h2>
                        <AccordionButton className={style.accordion_button}>
                            <Box flex="1" textAlign="left" className={style.menu_font}>
                            Menu
                            </Box>
                            <AccordionIcon />
                        </AccordionButton>
                        </h2>
                        <AccordionPanel pb={4}>
                        <Table w="100%" variant="simple" className={style.menu_font +" "+ style.table} size="xs">
                            <Thead className={style.thead}>
                                <Tr className={style.tr}>
                                <Th>Food</Th>
                                <Th isNumeric>Quantity</Th>
                                <Th isNumeric>Price</Th>
                                </Tr>
                            </Thead>
                            {data.list_barang.map((barang, index) => 
                                <Tbody key={index}>
                                    <Tr key={`${index}`+data.id+1}>
                                    <Td key={`${index}`+data.id+3}>{barang.nama_product}</Td>
                                    <Td key={`${index}`+data.id+5} isNumeric>{barang.quantity}</Td>
                                    <Td key={`${index}`+data.id+7} isNumeric>{barang.harga}</Td>
                                    </Tr>
                                </Tbody>
                            )}
                            <Tbody>
                                <Tr>
                                <Td></Td>
                                <Td isNumeric></Td>
                                <Td isNumeric>Rp {data.total_harga}</Td>
                                </Tr>
                            </Tbody>
                        </Table>
                        </AccordionPanel>
                    </AccordionItem>
                </Accordion>
            </Box>
        </Box>
    )
}

export default UserOrderHistoryCard
