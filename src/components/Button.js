import React from 'react';
import '../css/login.css';
import { Button as ChakraButton } from '@chakra-ui/react';

const Button = props => {
  return (
    <ChakraButton className={props.className} onClick={props.onClick} props>
      {props.children}
    </ChakraButton>
  );
};

export default Button;
