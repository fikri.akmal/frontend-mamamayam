import axios from 'axios'

const fetchHistory = async (username) => {
    const url = 'https://mamayam-backend.herokuapp.com/api/cart/user-order-history-with-barang-object/?username='+username;
    // const url = 'http://localhost:8000/api/cart/user-order-history/?username='+username;
    const response = await axios.get(url)
    return response
}

export default fetchHistory;
