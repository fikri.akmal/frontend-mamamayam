import axios from 'axios'

export default axios.create({
    baseURL:'https://mamayam-backend.herokuapp.com/api/'
});