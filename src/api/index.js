import fetchHistory from "./fetchHistory";
import fetchProductById from "./fetchProductById";



export {
    fetchHistory,
    fetchProductById
}